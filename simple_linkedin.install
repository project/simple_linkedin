<?php

/**
 * Implements hook_install().
 */
function simple_linkedin_install() {
  $field = array(
    'field' => array(
      'field_name'  => 'field_linkedin',
      'label'       => 'Set as default',
      'type'        => 'list_boolean',
      // It's mandatory to add at least two empty values here,
      // Drupal will do the rest then:
      'settings'    => array(
        'allowed_values' => array(
          '',
          '',
        ),
      ),
    ),
    'instance' => array(
      'field_name'  => 'field_linkedin',
      'entity_type' => 'node',
      'bundle'      => 'article',
      'label'       => 'Announce on LinkedIn',
      'widget'      => array(
        'type'      => 'options_onoff',
        'settings'  => array(
          'display_label' => TRUE,
        ),
      ),
      'display'     => array(
        'default'   => array(
          'label'   => 'hidden',
          'type'    => 'hidden',
        ),
      ),
    ),
  );
  if (!field_info_field($field['field'])) {
    field_create_field($field['field']);
  }
  $node_types = variable_get('linkedin_content_types');

  foreach ($node_types as $node_type) {
    if ($node_type){
      $field['instance']['bundle'] = $node_type;
      field_create_instance($field['instance']);
    }
  }
}

/**
 * Implements hook_schema().
 */
function simple_linkedin_schema() {
  $schema['linkedin_token_auto'] = array(
    'description' => 'Tokens for request and services accesses.',
    'fields' => array(
      'uid' => array(
        'description' => 'User ID from {user}.uid.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'token_key' => array(
        'description' => 'Tokens for request and services accesses.',
        'type' => 'varchar',
        'length' => 400,
        'not null' => TRUE,
      ),
      'token_secret' => array(
        'description' => 'Token "password".',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'Type of the token: request or access.',
        'type' => 'varchar',
        'length' => 7,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('uid'),
    'indexes' => array(
      'token_key_type' => array('token_key'),
    ),
  );
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function simple_linkedin_uninstall() {
  $node_types = node_type_get_types();
  foreach ($node_types as $node_type) {
    $instance = array(
      'field_name' => 'field_linkedin',
      'entity_type' => 'node',
      'bundle' => $node_type->type,
    );
    field_delete_instance($instance);
  }
  field_delete_field('field_linkedin');
}
