<?php

/**
 * @file
 */

/**
 * Linkedin settings form.
 */
function linkedin_autopost_settings() {
  global $base_url;

  $form = array();

  $check_lib = libraries_load('linkedin-api-client');
  if (!$check_lib['loaded']) {
    drupal_set_message(t('Library could not be loaded.
    Please install <a href="https://github.com/Happyr/LinkedIn-API-client" target="blank">LinkedIn API client</a>. </br>
    Create the folder libraries/linkedin-api-client, then go to the folder and execute the command:</br>
    composer require php-http/curl-client guzzlehttp/psr7 php-http/message happyr/linkedin-api-client'), 'error');
  }

  $check = strlen((variable_get('linkedin_autopost_clien_id', '')) == 0 || strlen(variable_get('linkedin_autopost_clien_secret_key', '')) == 0) ? TRUE : FALSE;
  if ($check) {
    drupal_set_message(t('Create a !link and set the Clien ID and Secret key for the App created.', array(
      '!link' => l(t('Linked app'), 'https://www.linkedin.com/developer/apps')
    )), 'warning');
  }

  $form['linkedin_autopost_clien_id'] = array(
    '#title' => t('Client ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('linkedin_autopost_clien_id', ''),
    '#description' => t('You will get this values by requesting an API key at https://www.linkedin.com/secure/developer.'),
  );
  $form['linkedin_autopost_clien_secret_key'] = array(
    '#title' => t('Secret key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('linkedin_autopost_clien_secret_key', ''),
    '#description' => t('You will get this values by requesting an API key at https://www.linkedin.com/secure/developer.'),
  );
  $form['linkedin_autopost_list'] = array(
    '#type' => 'checkbox',
    '#title' => t('Company'),
    '#default_value' => variable_get('linkedin_autopost_list', 0),
    '#description' => t('Please select this checkbox if we want to send post to company.'),
  );
  $form['linkedin_autopost_clien_company_id'] = array(
    '#title' => t('Company ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('linkedin_autopost_clien_company_id', ''),
    '#states' => array(
      'invisible' => array(
        ':input[name="linkedin_autopost_list"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['linkedin_info'] = array(
    '#markup' => 'Linkedin <b>redirect</b> url - ' . $base_url . request_uri() . '</br>',
    //'#description' => t('Notice that the "integration url" you specify at Linkedin must match the base url (https://bbsradio.com).'),
  );

  $info_coulumns = array();

  $form['linkedin_content_types'] = array(
    '#type' => 'checkboxes',
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('linkedin_content_types', $info_coulumns),
    '#title' => t('What content type need to link with linkedin ?'),
  );

  $form['#submit'][] = 'submit_action_linkedin_conf';


  // Find all node content types.
  $node_types = node_type_get_types();

  $form_item_preffix = 'linkedin_conf_';

  // Loop through each content type and give it options.
  foreach ($node_types as $key => $value) {

    // Store image fields in an options array.
    $image_fields = array();

    // Do the same for text fields.
    $text_fields = array();

    // Get fields of each content type and loop through to find image fields.
    $fields_info = field_info_instances('node', $key);
    foreach ($fields_info as $field_name => $data) {
      $field_info = field_info_field($field_name);

      switch ($field_info['type']) {
        case 'image':
          $image_fields[$field_info['field_name']] = $data['label'];
          break;

        case 'text':
        case 'text_long':
        case 'text_with_summary':
          $text_fields[$field_info['field_name']] = $data['label'];
          break;
      }
    }

    // Simple fieldset to wrap the content type options.
    $form[$key] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t(check_plain($value->name)),
    );

    $form[$key][$form_item_preffix . $key . '_image_field'] = array(
      '#title' => t('Select image field'),
      '#description' => t('Which image field on this content type should be used as the preview image on Linkedin. If there is no field or one is not selected then the site logo will be used.'),
      '#type' => 'select',
      '#options' => $image_fields,
      '#default_value' => variable_get($form_item_preffix . $key . '_image_field', ''),
    );

    $form[$key][$form_item_preffix . $key . '_text_field'] = array(
      '#title' => t('Select text field'),
      '#description' => t('Which text field on this content type should be used as the summary text on Linkedin. If there is no field or one is not selected then the site name will be used.'),
      '#type' => 'select',
      '#options' => $text_fields,
      '#default_value' => variable_get($form_item_preffix . $key . '_text_field', variable_get('site_name')),
    );

  }

  return system_settings_form($form);
}

function linkedin_user_settings(){
  global $user;
  global $base_url;
  $account = $user;
  $uid = $account->uid;
  //_linkedin_autopost_profile_settings_import();
  $check_lib = libraries_load('linkedin-api-client');
  if (!$check_lib['loaded']) {
    drupal_set_message(t('Library could not be loaded.
    Please install <a href="https://github.com/Happyr/LinkedIn-API-client" target="blank">LinkedIn API client</a>. </br>
    Create the folder libraries/linkedin-api-client, then go to the folder and execute the command:</br>
    composer require php-http/curl-client guzzlehttp/psr7 php-http/message happyr/linkedin-api-client'), 'error');
  }


  $form = array();
  $form['#user'] = $uid;

  if (isset($_GET['code'])) {
    variable_set('linkedin_code', $_GET['code']);
    $data = array(
      'grant_type' => 'authorization_code',
      'code' => $_GET['code'],
      'redirect_uri' => $base_url . '/admin/config/services/user_linkedin_autopost',
      'client_id' => variable_get('linkedin_autopost_clien_id', ''),
      'client_secret' => variable_get('linkedin_autopost_clien_secret_key', ''),
    );

    $options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($data),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    $result = drupal_http_request('https://www.linkedin.com/oauth/v2/accessToken', $options);
    $token = isset($result->data) ? json_decode($result->data) : FALSE;
    $tokenKey = isset($token->access_token) ? $token->access_token : FALSE;
    $tokenSecret = isset($_GET['code']) ? $_GET['code'] : FALSE;
    if (isset($tokenKey)) {
      //Check if the same LinkedIn account is not already tied to another user
      $result = db_query("SELECT uid FROM {linkedin_token_auto} WHERE token_key = :token_key AND token_secret = :token_secret AND type = :type ", array(':token_key' => $tokenKey, ':token_secret' => $tokenSecret, ':type' => 'access'))->fetchField();
      if ($result) {
        $registered = user_load($result);
        drupal_set_message(t('Sorry, this LinkedIn account is already associated with user !registered', array('!registered' => l($registered->name, 'user/' . $result))), 'warning');
        drupal_goto('admin/config/services/user_linkedin_autopost');
      }
      //save acces token for future requests
      $sql = array(
        'uid' => $uid,
        'token_key' => $tokenKey,
        'token_secret' => $tokenSecret,
        'type' => 'access',
      );
      //drupal_write_record('linkedin_token', $sql, array('uid'));

      $return_value = db_merge('linkedin_token_auto')
        ->key(array('uid' => $sql['uid']))
        ->fields($sql)
        ->execute();

      drupal_goto("admin/config/services/user_linkedin_autopost");
    }
  }

  if ($check_lib['loaded']) {
    $linkedIn = new Happyr\LinkedIn\LinkedIn(variable_get('linkedin_autopost_clien_id', ''), variable_get('linkedin_autopost_clien_secret_key', ''));
  }

  $row = db_query("SELECT * FROM {linkedin_token_auto} WHERE uid = :uid AND type = :type", array(':uid' => $uid, ':type' => 'access'))->fetchAssoc();
  if (isset($row['token_key'])) {
    /*$form['linked_token'] = array(
      '#markup' => '<div>Login Token: ' . $row['token_key'] . '</div>',
      '#weight' => 50,
    );*/
    $linkedIn->setAccessToken($row['token_key']);
  }

  if ($check_lib['loaded'] && $linkedIn->isAuthenticated()) {
    $userDetail = $linkedIn->get('v1/people/~:(first-name ,public-profile-url ,lastName)');
    $form['linked_user'] = array(
      '#markup' => '<div>' . "Your account is associated with <strong>" . $userDetail['firstName']
        . ' '. $userDetail['lastName']. '</strong> '.
      '<a href='. $userDetail['publicProfileUrl']. '>'. $userDetail['publicProfileUrl'] .'</a></div>',
      '#weight' => 50,
    );
    $form['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Unlink Linkedin'),
      '#submit' => array('linkedin_autopost_reset'),
    );
  }else{
    $url = $linkedIn->getLoginUrl();
    $form['linked_url'] = array(
      '#markup' => l(t('Login with LinkedIn'), $url),
      '#weight' => 50,
      '#suffix' => '<p class="description">' . t('You will be taken to the LinkedIn website in order to complete the process.') . '</p>',
      '#prefix' => '<p class="description">' . t('You must first authorize LinkedIn integration to use related features.') . '</p>',
    );
  }

  return $form;
}

function linkedin_autopost_reset($form, &$form_state) {
  //$form_state['rebuild'] = FALSE;

  global $user;
  $account = $user;

  // Delete token when user is deleted
  db_delete('linkedin_token_auto')
    ->condition('uid', $account->uid)
    ->execute();

  drupal_goto('admin/config/services/user_linkedin_autopost');

}

function _linkedin_autopost_profile_settings_import(){

  global $base_url;
  global $user;
  $account = $user;


}

function submit_action_linkedin_conf($form, &$form_state){

  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  $field = array(
    'field' => array(
      'field_name'  => 'field_linkedin',
      'label'       => 'Set as default',
      'type'        => 'list_boolean',
      // It's mandatory to add at least two empty values here,
      // Drupal will do the rest then:
      'settings'    => array(
        'allowed_values' => array(
          '',
          '',
        ),
      ),
    ),
    'instance' => array(
      'field_name'  => 'field_linkedin',
      'entity_type' => 'node',
      'bundle'      => 'article',
      'label'       => 'Announce on LinkedIn',
      'widget'      => array(
        'type'      => 'options_onoff',
        'settings'  => array(
          'display_label' => TRUE,
        ),
      ),
      'display'     => array(
        'default'   => array(
          'label'   => 'hidden',
          'type'    => 'hidden',
        ),
      ),
    ),
  );
  if (!field_info_field($field['field'])) {
    field_create_field($field['field']);
  }

  $node_types = $form_state['values']['linkedin_content_types'];

  foreach ($node_types as $key => $node_type) {
    if ($node_type && empty($instance = field_info_instance('node', 'field_linkedin', $key)) ){
      $field['instance']['bundle'] = $key;
      field_create_instance($field['instance']);
    }else if (!$node_type && !empty($instance = field_info_instance('node', 'field_linkedin', $key))){
      $instance = array(
        'field_name' => 'field_linkedin',
        'entity_type' => 'node',
        'bundle' => $key,
      );
      field_delete_instance($instance);
    }
  }
}
