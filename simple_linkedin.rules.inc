<?php

/**
 * @file
 */

/**
 * Implement hook_rules_action_info().
 */
function simple_linkedin_rules_action_info() {
  $actions = array(
    'linkedin_post_action' => array(
      'label' => t('Send to linkedin'),
      'group' => t('linkedin'),
      'module' => 'linkedin_autopost',
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('Current Node')),
      ),
    ),
  );
  return $actions;
}

/**
 * Implements hook_rules_condition_info().
 */
function simple_linkedin_rules_condition_info() {
  return array(
    'simple_linkedin_node_published' => array(
      'group' => t('linkedin'),
      'label' => t('Current node published to linkedin'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Validate node if status published'),
        ),
      ),
    ),
  );
}

/**
 * Callback function for custom condition info.
 */
function simple_linkedin_node_published($node) {
  $linked = isset($node->field_linkedin['und'][0]['value']) ? $node->field_linkedin['und'][0]['value'] : 0;
  if ($linked) {
    $new = $node->status;
    $old = isset($node->original) ? $node->original->status : 0;
    if ($old == 0 && $new == 1) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * The action linkedin.
 */
function linkedin_post_action($node) {

  global $user;
  $account = $user;
  $uid = $account->uid;

  libraries_load('linkedin-api-client');
  $linkedIn = new Happyr\LinkedIn\LinkedIn(variable_get('linkedin_autopost_clien_id', ''), variable_get('linkedin_autopost_clien_secret_key', ''));

  $linkedin_token = db_query("SELECT token_key FROM {linkedin_token_auto} WHERE uid = :uid AND type = :type ", array(':uid' => $uid, ':type' => 'access'))->fetchField();

  $linkedIn->setAccessToken($linkedin_token);

  if ($linkedIn->isAuthenticated()) {

    $placeholders = simple_linkedin_li_placeholders($node);

    $post_fields = array(
      'json' =>
        array(
          'content' => array(
            'title' => $placeholders['!description'],
            'description' => "Worldwide Marketing and Global Syndication of Talk Radio programming! Try our Diamond level platform and live professional services at a price that can't be beat! We are dedicated, personable and always available! Call 888-710-8061", //$placeholders['!description'],
            'submitted-url' => $placeholders['!url'],
            'submitted-image-url' => $placeholders['!image'],
          ),
          'visibility' => array(
            'code' => 'anyone',
          ),
          'comment' => $placeholders['!title'],
        ),
    );

    // Change the fields before sending.
    //drupal_alter('linkedin_autopost', $post_fields);

    if(variable_get('linkedin_autopost_list', 0)){
      $url = 'v1/companies/'.variable_get('linkedin_autopost_clien_company_id', '').'/shares';
      $result = $linkedIn->post($url, $post_fields);

      watchdog('LinkedIn API-company:', '<pre>Output: @Output</pre>', array(
        '@Output' => print_r($result, TRUE),
      ), WATCHDOG_NOTICE);
    }

    $url = 'v1/people/~/shares';
    $result = $linkedIn->post($url, $post_fields);

    watchdog('LinkedIn API-Person', '<pre>Output: @Output</pre>', array(
      '@Output' => print_r($result, TRUE),
    ), WATCHDOG_NOTICE);
  }
}


function simple_linkedin_li_placeholders($node)
{
  global $user;
  global $base_url;
  //DG Edited.
  $nType = $node->type;
  $des = '';
 
	$fieldTagsarray = '';
	$linkedInTitle  = $node->title;
	$image_field_name = variable_get('linkedin_conf_'.$nType.'_image_field');
	if ($image_field_name && array_filter($node->{$image_field_name})) {
	$file    = file_load($node->{$image_field_name}['und'][0]['fid']);
	$uri     = $file->uri;
	$picture = file_create_url($uri);

	} else {
	$picture = '';

	}
	$desc_field_name = variable_get('linkedin_conf_'.$nType.'_text_field');
	$des= ($desc_field_name) ? $node->{$desc_field_name}['und'][0]['value'] : '';


  $placeholders = array(
    '!title' => $linkedInTitle,
    '!url' => url('node/' . $node->nid, array(
      'absolute' => TRUE
    )),
    '!user' => $user->name,
    '!site' => variable_get('site_name', $base_url),
    '!image' => $picture,
    '!description' => $des
  );
  //print "Placeholders <pre>"; print_r($placeholders); print "</pre>";
  return $placeholders;
}
